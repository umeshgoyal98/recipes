import { Component } from '@angular/core';
import { AuthService, AuthResponseData } from './auth.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
    selector:'app-auth',
    templateUrl:'./auth.component.html'
})
export class AuthComponent{
    isLoginMode=true;
    isLoading=false;
    error:string=null;

    constructor(private authService:AuthService,private router: Router){}

    onSwitchMode(){
        this.isLoginMode=!this.isLoginMode;
    }

    onSubmit(form:NgForm){
        if(!form.valid){
            return;
        }
        const email=form.value.email;
        const password=form.value.password;
        let authObs:Observable<AuthResponseData>
        if(this.isLoginMode){
            this.isLoading=true;
            authObs=this.authService.login(email,password);
        }else{
           
            this.isLoading=true;
            this.authService.signup(email,password);
        }
        authObs.subscribe(
            responseData=>{
                console.log(responseData);
                this.isLoading=false;
                this.router.navigate(['/recipe']);
            },
            errorMessage=>
            {
                
                this.error=errorMessage;
                this.isLoading=false;
            }
        );
        form.reset();
    }

}