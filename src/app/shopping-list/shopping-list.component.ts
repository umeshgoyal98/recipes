import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ingredient } from '../ingredient.model';
import { ShoppingListService } from './shopping-list.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit,OnDestroy {
  ingredients: Ingredient[];
  igchangedSub: Subscription;
  

  constructor(private shoppinglistService: ShoppingListService) { }
  

  ngOnInit() {
    this.ingredients=this.shoppinglistService.getIngredient();
    this.igchangedSub= this.shoppinglistService.ingredientChange.subscribe((ingredients: Ingredient[])=> {
      this.ingredients=ingredients;
});
  }

  onEditItem(index:number){
    this.shoppinglistService.startedEditing.next(index);

  }
  ngOnDestroy():void{
    this.igchangedSub.unsubscribe();
  }
}
