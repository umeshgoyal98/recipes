import { Component, OnInit ,ViewChild } from '@angular/core';
import { Ingredient } from 'src/app/ingredient.model';
import { ShoppingListService } from '../shopping-list.service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
   
  subscription:Subscription;
  editMode=false;
  editId:number;
  editIngredient: Ingredient;
  @ViewChild('f',{static: false}) slForm: NgForm;
  
  onAddedItem(form:NgForm){
    const value= form.value;
    const newIngredient= new Ingredient(value.name,value.amount);
    if(this.editMode){
      this.slService.updateIngredient(this.editId,newIngredient);
    }
    else{
    this.slService.addIngredient(newIngredient);
    }
    this.editMode=false;
    form.reset();
  }

  constructor(private slService: ShoppingListService) { }

  ngOnInit() {
    this.subscription=this.slService.startedEditing.subscribe(
      (index:number) => {
        this.editId=index;
        this.editMode=true;
        this.editIngredient=this.slService.getIngredients(index);
        this.slForm.setValue({
        name: this.editIngredient.name,
        amount: this.editIngredient.amount
      });

  });
}
onDelete(){
  this.slService.delete(this.editId);
  this.onClear();

}
  onClear(){
    this.slForm.reset();
    this.editMode=false;
  }

}
