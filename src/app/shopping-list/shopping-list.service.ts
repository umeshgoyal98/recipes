import { Ingredient } from '../ingredient.model';
import { EventEmitter } from '@angular/core';
import {Subject} from 'rxjs';
export class ShoppingListService {
    ingredientChange = new Subject<Ingredient[]>();
    startedEditing=new Subject<number>();

   private ingredients: Ingredient[]=[new Ingredient('Apple',5),new Ingredient('Tamato',10)];

   getIngredient(){
       return this.ingredients.slice();
   }

   addIngredient(ingredient: Ingredient){
       this.ingredients.push(ingredient);
       this.ingredientChange.next(this.ingredients.slice());
   }
   getIngredients(index: number){
       return this.ingredients[index];
   }
    
   addIngredients(ingredients: Ingredient[]){
       this.ingredients.push(...ingredients);
       this.ingredientChange.next(this.ingredients.slice());
   }
   updateIngredient(index: number, newIngredient : Ingredient){
        this.ingredients[index]=newIngredient;
        this.ingredientChange.next(this.ingredients.slice());
   }
   delete(index: number){
       this.ingredients.splice(index,1);
       this.ingredientChange.next(this.ingredients.slice());
   }
}