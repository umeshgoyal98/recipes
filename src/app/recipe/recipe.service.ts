import { Recipe } from './recipe.model';
import {  Injectable } from '@angular/core';
import { Ingredient } from '../ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Subject } from 'rxjs';

@Injectable()
export class RecipeService {
    recipesChanges= new Subject<Recipe[]>();
  
  //     recipes: Recipe[]=[
  //       new Recipe('FarmHouse Pizza',
  //       'This food is too delicious.',
  //       'https://media.gettyimages.com/photos/pizza-picture-id184946701?s=2048x2048',
  //       [new Ingredient('Bread',1),
  //     new Ingredient('olives',10)]),
  //   new Recipe('Onion Pizza',
  //   'This food is too delicious.',
  //   'https://media.gettyimages.com/photos/closeup-of-pizza-on-table-picture-id995467932?s=2048x2048',
  //   [new Ingredient('Bread',1),
  // new Ingredient('Onion',2)])];
recipes:Recipe[]=[];
  constructor(private slService: ShoppingListService){}

  setRecipes(recipes:Recipe[]){
    this.recipes=recipes;
    this.recipesChanges.next(this.recipes.slice());
  }
  getRecipes(){
    return this.recipes.slice();
  }
  getRecipeById(id:number){
    return this.recipes.slice()[id];
  }
  addIngredientToShoppingList(ingredients: Ingredient[]){
    this.slService.addIngredients(ingredients);
  }
  addRecipe(recipe: Recipe){
    this.recipes.push(recipe);
    this.recipesChanges.next(this.recipes.slice());

  }
  updateRecipe(index: number,newRecipe: Recipe){
    this.recipes[index]=newRecipe;
    this.recipesChanges.next(this.recipes.slice());
  }
  deleteRecipe(index: number){
    this.recipes.splice(index,1);
    this.recipesChanges.next(this.recipes.slice());
  }
}