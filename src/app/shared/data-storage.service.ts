import { Injectable } from '@angular/core';
import { RecipeService } from '../recipe/recipe.service';
import { Recipe } from '../recipe/recipe.model';
import { HttpClient } from '@angular/common/http';
import {map, tap} from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';

@Injectable({providedIn:'root'})
export class DataStorageService{

    constructor(private recipeService: RecipeService,
        private http: HttpClient,private authService:AuthService){}
    storeData(){
        const recipes: Recipe[]=this.recipeService.getRecipes();
        this.http.put('https://ng-course-recipe-to-success.firebaseio.com/recipes.json',recipes).subscribe(
            response=>{
                console.log(response);
            }
        );

    }

    fetchData(){

        return this.http.get<Recipe[]>('https://ng-course-recipe-to-success.firebaseio.com/recipes.json').pipe(
            map(recipes=>{
                return recipes.map(recipe=>{
                    return {...recipe,ingredients:recipe.ingredients? recipe.ingredients:[]};
                });
            }),
            tap(recipes=>{
                this.recipeService.setRecipes(recipes);

            })
        );
    }

}