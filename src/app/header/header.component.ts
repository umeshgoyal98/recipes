import { Component, EventEmitter, Output, OnInit, OnDestroy } from '@angular/core';
import { DataStorageService } from '../shared/data-storage.service';
import { AuthService } from '../auth/auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
    selector:'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit,OnDestroy{
    subs: Subscription;
    isAuthenticated=false;

   constructor(private dataStorageService: DataStorageService,private authService: AuthService,
    private router: Router){}

   ngOnInit(){
        this.subs=this.authService.user.subscribe(
            user=>{
                this.isAuthenticated=!!user;
            }
        );
   }
    onSaveData(){
        this.dataStorageService.storeData();
    }

    onFetchData(){
        this.dataStorageService.fetchData().subscribe();
    }
    logout(){
        this.authService.logout();
            }

    ngOnDestroy(){
        this.subs.unsubscribe();
    }
}